/**
@file o3o.c

@brief writeme

Copyright (C) 2015 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "o3o.h"

/**
@brief about

writeme

@param stack about
@param value about
@return writeme
*/

static o3o_status_t o3oPush(o3o_stack_s *stack, double value)
{
  void *tmp;
  if (!stack) { return O3O_STATUS_NULLPOINTER; }
  tmp = realloc(stack->data, sizeof(double) * (stack->size + 1));
  if (!tmp) { return O3O_STATUS_OUTOFMEMORY; }
  stack->data = (double*)tmp;
  stack->data[stack->size] = value;
  ++stack->size;
  return O3O_STATUS_OK;
}

/**
@brief about

writeme

@param stack about
@return writeme
*/

static double o3oPeek(o3o_stack_s *stack)
{ return stack && stack->size ? stack->data[stack->size - 1] : 0; }

/**
@brief about

writeme

@param stack about
@return writeme
*/

static double o3oPop(o3o_stack_s *stack)
{
  double value = o3oPeek(stack);
  if (stack && stack->size) { --stack->size; }
  return value;
}

/**
@brief about

This is only used internally; use o3oLoadText() or o3oLoadFile() instead.

@param buflen about
@return writeme
*/

static o3o_vm_s *o3oInit(size_t buflen)
{

  o3o_vm_s *o3o = NULL;

  if (buflen < 1) { return NULL; }
  o3o = (o3o_vm_s*)malloc(sizeof(o3o_vm_s));
  if (!o3o) { return NULL; }
  memset(o3o, 0, sizeof(o3o_vm_s));

  o3o->code = (char*)malloc(buflen + 1);
  if (!o3o->code) { return o3oFree(o3o); }
  return o3o;

}

/**
@brief about

writeme

@param code about
@return writeme
*/

o3o_vm_s *o3oLoadText(const char *code)
{
  o3o_vm_s *o3o = NULL;
  if (!code) { return NULL; }
  o3o = o3oInit(strlen(code));
  if (o3o) { strcpy(o3o->code, code); }
  return o3o;
}

/**
@brief about

writeme

@param path about
@return writeme
*/

o3o_vm_s *o3oLoadFile(const char *path)
{

  o3o_vm_s *o3o = NULL;
  size_t len = 0;
  FILE *src = NULL;

  src = fopen(path, "rb");
  if (!src) { return NULL; }

  fseek(src, 0, SEEK_END);
  len = ftell(src);
  fseek(src, 0, SEEK_SET);

  if ((o3o = o3oInit(len))) { fread(o3o->code, 1, len, src); }

  fclose(src);
  return o3o;

}

/**
@brief about

writeme

@param o3o about
@return writeme
*/

o3o_vm_s *o3oFree(o3o_vm_s *o3o)
{
  o3o_math_t i = 0;
  if (!o3o) { return NULL; }
  for (i = 0; i < O3O_NUM_MATH; ++i) { free(o3o->stack[i].data); }
  free(o3o->code);
  free(o3o);
  return NULL;
}

/**
@brief about

writeme

@param o3o about
@return writeme
*/

o3o_status_t o3oReset(o3o_vm_s *o3o)
{

  o3o_math_t i = 0;

  if (!o3o || !o3o->code) { return O3O_STATUS_NULLPOINTER; }

  for (i = 0; i < O3O_NUM_MATH; ++i)
  {
    free(o3o->stack[i].data);
    o3o->stack[i].data = NULL;
    o3o->stack[i].size = 0;
  }

  o3o->flag = 0;
  o3o->ip = 0;

  return o3oInput(o3o, NULL);

}

/**
@brief about

writeme

@param o3o about
@param text about
@return writeme
*/

o3o_status_t o3oInput(o3o_vm_s *o3o, const char *text)
{

  o3o_stack_s *stack = NULL;
  size_t i = 0;

  if (!o3o || !o3o->code)                { return O3O_STATUS_NULLPOINTER; }
  else if (o3o->ip >= strlen(o3o->code)) { return O3O_STATUS_OUTOFBOUNDS; }

  for (; o3o->code[o3o->ip]; ++o3o->ip)
  {

    switch (o3o->code[o3o->ip])
    {

      case '+': stack = &o3o->stack[O3O_MATH_ADD]; break;
      case '-': stack = &o3o->stack[O3O_MATH_SUB]; break;
      case '*': stack = &o3o->stack[O3O_MATH_MUL]; break;
      case '/': stack = &o3o->stack[O3O_MATH_DIV]; break;
      case '%': stack = &o3o->stack[O3O_MATH_MOD]; break;
      case '_': stack = &o3o->stack[O3O_MATH_NOP]; break;

      /* Update all stacks (except for the no-op stack, of course). */
      case '@':
      {
        double f = o3oPeek(&o3o->stack[O3O_MATH_NOP]);
        for (i = 0; i < o3o->stack[O3O_MATH_ADD].size; ++i) { o3o->stack[O3O_MATH_ADD].data[i] += f; }
        for (i = 0; i < o3o->stack[O3O_MATH_SUB].size; ++i) { o3o->stack[O3O_MATH_SUB].data[i] -= f; }
        for (i = 0; i < o3o->stack[O3O_MATH_MUL].size; ++i) { o3o->stack[O3O_MATH_MUL].data[i] *= f; }
        for (i = 0; i < o3o->stack[O3O_MATH_DIV].size; ++i) { o3o->stack[O3O_MATH_DIV].data[i] /= f; }
        for (i = 0; i < o3o->stack[O3O_MATH_MOD].size; ++i) { o3o->stack[O3O_MATH_MOD].data[i] = fmod(o3o->stack[O3O_MATH_MOD].data[i], f); }
      }
      continue;

      case '[':
        if (!o3o->flag)
        {
          for (i = 1; i; ++o3o->ip)
          {
            if      (!o3o->code[o3o->ip+1]) { return O3O_STATUS_OUTOFBOUNDS; }
            else if (o3o->code[o3o->ip+1] == '[') { ++i; }
            else if (o3o->code[o3o->ip+1] == ']') { --i; }
          }
        }
        o3o->flag = 0;
      continue;

      case '}':
        if (o3o->flag)
        {
          for (i = 1; i; --o3o->ip)
          {
            if      (o3o->ip == 0) { return O3O_STATUS_OUTOFBOUNDS; }
            else if (o3o->code[o3o->ip-1] == '{') { --i; }
            else if (o3o->code[o3o->ip-1] == '}') { ++i; }
          }
        }
        o3o->flag = 0;
      continue;

      /* Unknown opcode; ignored. */
      default: continue;

    }

    /* Various things can come after the initial stack specifier... */
    ++o3o->ip;
    switch (o3o->code[o3o->ip])
    {

      case '+': o3oPush(stack, o3oPeek(&o3o->stack[O3O_MATH_ADD])); break;
      case '-': o3oPush(stack, o3oPeek(&o3o->stack[O3O_MATH_SUB])); break;
      case '*': o3oPush(stack, o3oPeek(&o3o->stack[O3O_MATH_MUL])); break;
      case '/': o3oPush(stack, o3oPeek(&o3o->stack[O3O_MATH_DIV])); break;
      case '%': o3oPush(stack, o3oPeek(&o3o->stack[O3O_MATH_MOD])); break;
      case '_': o3oPush(stack, o3oPeek(&o3o->stack[O3O_MATH_NOP])); break;

      case '=': o3o->flag |= o3oPeek(stack) == o3oPeek(&o3o->stack[O3O_MATH_NOP]); break;
      case '>': o3o->flag |= o3oPeek(stack) >  o3oPeek(&o3o->stack[O3O_MATH_NOP]); break;
      case '<': o3o->flag |= o3oPeek(stack) <  o3oPeek(&o3o->stack[O3O_MATH_NOP]); break;

      /* Pop a value from the stack, optionally printing it to stdout. */
      case '.':                   o3oPop(stack);  break;
      case '?': printf("%f",      o3oPop(stack)); break;
      case '!': printf("%c", (int)o3oPop(stack)); break;

      /* Read a number (or a string of characters) from text + push it/them. */
      case '#': case '$':
        if (!text) { --o3o->ip; return O3O_STATUS_WAITFORTEXT; }
        if       (o3o->code[o3o->ip] == '#'   ) { o3oPush(stack, atof(text)); }
        else for (i = strlen(text) + 1; i; --i) { o3oPush(stack, text[i-1]);  }
        text = NULL;
      break;

      /* This must be an assignment from a literal... */
      default:
      {
        const char *txt_s = &o3o->code[o3o->ip];
        char *txt_e = NULL;
        o3oPush(stack, strtod(txt_s, &txt_e));
        if (!txt_e) { return O3O_STATUS_PARSERERROR; }
        for (--o3o->ip; txt_s != txt_e; ++o3o->ip) { ++txt_s; }
      }
      break;

    }

  }

  return O3O_STATUS_OK;

}

/*
[PUBLIC] about
*/

const char *o3oStatus(o3o_status_t status)
{

  switch (status)
  {
    case O3O_STATUS_OK:          return "Success";
    case O3O_STATUS_WAITFORTEXT: return "Waiting for input";
    case O3O_STATUS_PARSERERROR: return "Unexpected token";
    case O3O_STATUS_NULLPOINTER: return "Null pointer";
    case O3O_STATUS_OUTOFMEMORY: return "Out of memory";
    case O3O_STATUS_OUTOFBOUNDS: return "Instruction pointer out of bounds";
    default: break;
  }

  return "Unknown error";

}
