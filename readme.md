o3o - A Stack-Based Programming Language
========================================

The o3o programming language replaces variables and mathematical operators with
numeric stacks. Each stack can store an arbitrary amount of numeric values. The
contents of each stack are changed based on its "role" when an "update" occurs.

Number stacks
-------------

There are six stacks available:

+ `+` Add
+ `-` Subtract
+ `*` Multiply
+ `/` Divide
+ `%` Modulo
+ `_` No-op ("comparison")

All variables are IEEE double-precision floats, although they will be converted
into integers if they're used to output a character.

Basic assignment
----------------

To push a value on to a stack, simply omit any operator after the stack name:

    [stack][stack|value]
    
Stack operators
---------------

These commands are used for I/O:

    [stack]# Read line from stdin; parse it as a number + push it.
    [stack]$ Read line from stdin; push each character value in reverse order.
    [stack]. Pop a value from the stack and discard it.
    [stack]? Pop a value and output it as a number.
    [stack]! Pop a value and output it as a character.

Logical operators
-----------------

These commands are used to update the flag used by conditional code sections or
loops.

    [stack][logic]

+ `<` Less than.
+ `>` More than.
+ `=` Equal.

The topmost value of the specified stack is compared with the topmost value on
the "no-op" (`_`) stack.

Loops and conditions
--------------------

NOTE: Conditionals/loops can be nested.

`[` - Begin conditional section, based on ORd results of previous logic statements.
`]` - End conditional section.

`{` - Begin "do" loop.
`}` - End "do" loop; jump back to start if flag is set.

Note: The `[` and `}` characters reset the flag to `false` after it has been checked.

The "update" operator
---------------------

`@` - Update all stacks based the topmost value of the `_` stack.
