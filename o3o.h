/**
@file o3o.h

@brief writeme

Copyright (C) 2015 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __O3O_H__
#define __O3O_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h> /* size_t */

/**
@brief about

writeme
*/

typedef enum
{

  O3O_STATUS_OK,          /**< @brief about */
  O3O_STATUS_WAITFORTEXT, /**< @brief about */
  O3O_STATUS_PARSERERROR, /**< @brief about */
  O3O_STATUS_NULLPOINTER, /**< @brief about */
  O3O_STATUS_OUTOFMEMORY, /**< @brief about */
  O3O_STATUS_OUTOFBOUNDS  /**< @brief about */

} o3o_status_t;

/**
@brief about

writeme
*/

typedef enum
{

  O3O_MATH_NOP, /**< @brief about */
  O3O_MATH_ADD, /**< @brief about */
  O3O_MATH_SUB, /**< @brief about */
  O3O_MATH_MUL, /**< @brief about */
  O3O_MATH_DIV, /**< @brief about */
  O3O_MATH_MOD, /**< @brief about */
  O3O_NUM_MATH  /**< @brief about */

} o3o_math_t;

/**
@brief about

writeme
*/

typedef struct
{

  size_t size; /**< @brief about */
  double *data;       /**< @brief about */

} o3o_stack_s;

/**
@brief about

writeme
*/

typedef struct
{

  unsigned char flag;  /**< @brief about */
  size_t ip;     /**< @brief about */
  char          *code;  /**< @brief about */

  o3o_stack_s stack[O3O_NUM_MATH];  /**< @brief about */

} o3o_vm_s;

/**
@brief about

writeme

@param code about
@return writeme
*/

o3o_vm_s *o3oLoadText(const char *code);

/**
@brief about

writeme

@param path about
@return writeme
*/

o3o_vm_s *o3oLoadFile(const char *path);

/**
@brief about

writeme

@param o3o about
@return writeme
*/

o3o_vm_s *o3oFree(o3o_vm_s *o3o);

/**
@brief about

writeme

@param o3o about
@return writeme
*/

o3o_status_t o3oReset(o3o_vm_s *o3o);

/**
@brief about

writeme

@param o3o about
@param text about
@return writeme
*/

o3o_status_t o3oInput(o3o_vm_s *o3o, const char *text);

/**
@brief about

writeme

@param status about
@return writeme
*/

const char *o3oStatus(o3o_status_t status);

#ifdef __cplusplus
}
#endif

#endif  /* __O3O_H__ */
