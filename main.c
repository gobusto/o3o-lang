/**
@mainpage The o3o Programming Language

o3o is a simple programming language based around the use of "number stacks" in
place of mathematical operations.

TODO: Write more here...
*/

/**
@file main.c

@brief This file provides a basic command-line interpreter for O3O scripts.

Copyright (C) 2015 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include "o3o.h"

/**
@brief This is where the program begins.

@param argc Length of the argv array.
@param argv Program arguments.
@return Zero on success or non-zero on failure.
*/

int main(int argc, char *argv[])
{

  o3o_vm_s *o3o = NULL;
  o3o_status_t status;
  char text[512];
  int i = 0;

  if (argc < 2) { printf("Usage: %s [filename]\n", argv[0]); return 0; }

  for (i = 1; i < argc; ++i)
  {
    o3oFree(o3o);
    o3o = o3oLoadFile(argv[i]);
    if (!o3o) { printf("Could not load %s\n", argv[i]); return 0; }
  }

  for (status = o3oReset(o3o); status == O3O_STATUS_WAITFORTEXT; )
  {
    if (fgets(text, 512, stdin))
    {

      for (i = 0; text[i]; ++i)
      {
        if (text[i] == '\n' || text[i] == '\r') { text[i] = 0; break; }
      }

      status = o3oInput(o3o, text);

    } else { status = O3O_STATUS_NULLPOINTER; }
  }
  if (status != O3O_STATUS_OK) { printf("ERROR: %s\n", o3oStatus(status)); }

  o3oFree(o3o);
  return 0;

}
